package com.crypto.configuration;

import com.crypto.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class RepositoryConfig {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Bean
    public AccountRepository accountRepository(){
        return new AccountRepository(jdbcTemplate);
    }

}
