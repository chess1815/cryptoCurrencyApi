package com.crypto.configuration;

import com.crypto.controller.CoinController;
import com.crypto.service.BatchRunnable;
import com.crypto.service.CoinService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.crypto.controller.AdminController;
import com.crypto.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.crypto.controller.VersionController;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
@Import({ServiceConfig.class,ScheduleConfig.class})
public class ControllerConfig {

    @Autowired
    private AccountService accountService;

    @Autowired
    Jackson2ObjectMapperBuilder builder;

    @Autowired
    private CoinService coinService;

    @Autowired
    private BatchRunnable batchRunnable;

    @Bean
    public AdminController adminController() {
        return new AdminController(accountService,batchRunnable);
    }

    @Bean
    public VersionController versionController() {
        return new VersionController();
    }

    @Bean
    public CoinController coinController() {
        return new CoinController(coinService);
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//        objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        return objectMapper;
    }

}
