package com.crypto.controller;

import com.crypto.domain.Account;
import com.crypto.domain.BatchRunnableStatus;
import com.crypto.service.AccountService;
import com.crypto.service.BatchRunnable;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@AllArgsConstructor
public class AdminController {

    private final AccountService accountService;
    private BatchRunnable batchRunnable;


    @RequestMapping(value = "/account/create", method = RequestMethod.POST)
    @ResponseBody
    public Account getVersion(@RequestBody Account Account) {
        return accountService.createAccount(Account);
    }

    @RequestMapping(value = "/account/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Account getVersion(@PathVariable int id) {
        return accountService.getAccountById(id);
    }


    @RequestMapping(value = "/batch/on", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void turnOn() throws ExecutionException, InterruptedException {
        batchRunnable.turnOn();
    }

    @RequestMapping(value = "/batch/off", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void turnOff() throws ExecutionException, InterruptedException {
        batchRunnable.turnOff();
    }

    @RequestMapping(value = "/batch", method = RequestMethod.GET)
    @ResponseBody
    public BatchRunnableStatus batchStatus() throws ExecutionException, InterruptedException {
        return batchRunnable.getStatus();
    }

}
