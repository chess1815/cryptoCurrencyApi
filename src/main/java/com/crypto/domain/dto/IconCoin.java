package com.crypto.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IconCoin {

    @JsonProperty("ImageUrl")
    @NotNull
    private String imageUrl;

    @JsonProperty("CoinName")
    @NotNull
    private String coinName;

    @JsonProperty("Symbol")
    @NotNull
    private String symbol;

}
