package com.crypto.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


/*
CREATE TABLE ACCOUNT(
  ID   INT              NOT NULL,
  NAME VARCHAR (20)     NOT NULL,
  WECHAT_ID  CHAR (20)              NOT NULL,
  ADDRESS  VARCHAR (25) ,
  CREATE_TIME   TIMESTAMP NOT NULL,
  LAST_ACTIVITY_TIME TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (ID)
);
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountEntity extends GenericEntity{
    private String name;
    private String wechatId;
    private String address;
    private LocalDateTime createTime;
    private LocalDateTime lastActivityTime;

}
