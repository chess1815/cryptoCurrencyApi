package com.crypto.domain;

import com.crypto.domain.dto.AccountEntity;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Account {
    private int id;
    private String name;
    private String wechatId;
    private String address;
    private LocalDateTime createTime;
    private LocalDateTime lastActivityTime;


    public static Account deepCopy(Account account){
        return Account.builder().id(account.getId())
                .name(account.getName())
                .wechatId(account.getWechatId())
                .address(account.getAddress())
                .createTime(account.getCreateTime())
                .lastActivityTime(account.getLastActivityTime())
                .build();
    }

    public static Account buildFromEntity(AccountEntity entity){
        return entity!=null?Account.builder().id(entity.getId())
                .name(entity.getName())
                .wechatId(entity.getWechatId())
                .address(entity.getAddress())
                .createTime(entity.getCreateTime())
                .lastActivityTime(entity.getLastActivityTime())
                .build():null;
    }

    public static List<Account> buildFromEntity(List<AccountEntity> entities) {

       List<Account> accounts = new ArrayList<>();
        for(AccountEntity entity:entities){
            accounts.add(buildFromEntity(entity));
        }
        return accounts;


    }


}
