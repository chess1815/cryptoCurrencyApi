package com.crypto.service.client;

import com.crypto.domain.dto.CMCCoin;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Data
@Slf4j
public class CoinMarketCapClient {

    @Autowired
    private RestTemplate restTemplate;

    private String uri = "https://api.coinmarketcap.com/v1/ticker/?limit=1500";

    public List<CMCCoin> getCoinList() throws IOException {
        String cmcCoinsJson = restTemplate.getForObject(uri,String.class);
        log.info(cmcCoinsJson);
        ParameterizedTypeReference<List<CMCCoin>> typeRef = new ParameterizedTypeReference<List<CMCCoin>>() {
        };
        List<CMCCoin> cmcCoins = new ObjectMapper().readValue(cmcCoinsJson, new TypeReference<List<CMCCoin>>(){});

//        ResponseEntity<List<CMCCoin>> responseEntity = restTemplate.exchange(uri, HttpMethod.GET,null, typeRef);
        return cmcCoins;
    }


}
