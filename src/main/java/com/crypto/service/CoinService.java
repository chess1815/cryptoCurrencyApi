package com.crypto.service;

import com.crypto.domain.Coin;
import com.crypto.domain.CoinDescription;
import com.crypto.domain.dto.CMCCoin;
import com.crypto.domain.dto.CoinDescriptionEntity;
import com.crypto.domain.dto.CoinEntity;
import com.crypto.domain.dto.IconCoin;
import com.crypto.exception.CoinGeneralException;
import com.crypto.repository.CoinDesriptionRepository;
import com.crypto.repository.CoinRepository;
import com.crypto.service.client.CoinMarketCapClient;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@AllArgsConstructor
@Slf4j
@Transactional
public class CoinService {

    private final CoinRepository coinRepository;

    private final CoinDesriptionRepository coinDesriptionRepository;

    private final CoinMarketCapClient coinMarketCapClient;

    private final ResourceLoader resourceLoader;

    public List<Coin> getAllCoin() {
        List<Coin> coins = Coin.buildFromEntity(coinRepository.findAll());
        Collections.sort(coins);
        return coins;
    }

    public Coin getCoinById(long id) {
        return Coin.buildFromEntity(coinRepository.findOne(id));
    }

    public Coin getCoinByName(String name) {
        List<CoinEntity> coinEntities = coinRepository.findByName(name);

        if (coinEntities.size() == 1) {
            return Coin.buildFromEntity(coinEntities.get(0));
        } else if (coinEntities.size() == 0) {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "no coin found for " + name);
        } else {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "more than one coin found for " + name);
        }
    }

    public boolean updateDescriptionForCoin(long id, String coinName, String shortDescription, String longDescription) {
        List<CoinDescriptionEntity> coinDescriptionEntities = coinDesriptionRepository.findByName(coinName);
        if (coinDescriptionEntities.size() == 1) {
            coinDesriptionRepository.updateDescription(coinName, shortDescription, longDescription, Timestamp.valueOf(LocalDateTime.now()));
        } else if (coinDescriptionEntities.size() == 0) {
            CoinDescription coinDescription = CoinDescription.builder().id(id).name(coinName).longDescription(longDescription).shortDescription(shortDescription).createTime(LocalDateTime.now()).build();
            coinDesriptionRepository.save(CoinDescription.buildEntity(coinDescription));
        } else {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "more than one description found for " + coinName);
        }

        return true;
    }

    public CoinDescription getDescriptionForCoin(String coinName) {
        List<CoinDescriptionEntity> coinDescriptionEntities = coinDesriptionRepository.findByName(coinName);
        if (coinDescriptionEntities.size() == 1) {
            return CoinDescription.buildFromEntity(coinDescriptionEntities.get(0));
        } else if (coinDescriptionEntities.size() == 0) {
            return CoinDescription.builder().name(coinName).longDescription("").shortDescription("").build();
        } else {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "more than one description found for " + coinName);
        }
    }

    public boolean updateEnabled(CoinDescription coinDescription, boolean status) {
        coinRepository.updateEnabled(coinDescription.getId(), status);
        log.info("updateDescriptionForCoin");
        updateDescriptionForCoin(coinDescription.getId(),getCoinById(coinDescription.getId()).getName(), coinDescription.getShortDescription(), coinDescription.getLongDescription());
        return true;
    }

    protected Map<String, IconCoin> pullIcons() {

        try {
            TypeReference<HashMap<String, IconCoin>> typeRef
                    = new TypeReference<HashMap<String, IconCoin>>() {
            };
            String content = IOUtils.toString(resourceLoader.getResource("classpath:coinList.json").getInputStream(), "UTF-8");
            Map<String, IconCoin> map = new ObjectMapper().readValue(content, typeRef);
            return map;
        } catch (IOException e) {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "bad icon json file " + e);
        }

    }

    protected String getIconPath(Map<String, IconCoin> map, CMCCoin cmcCoin) {
        IconCoin iconCoin = map.get(cmcCoin.getSymbol());
        IconCoin iconCoinStar = map.get(cmcCoin.getSymbol() + "*");

        if (iconCoin != null && iconCoinStar == null && iconCoin.getSymbol().equalsIgnoreCase(cmcCoin.getSymbol())) {//best case
            return iconCoin.getImageUrl();
        } else if (iconCoin != null && (iconCoin.getCoinName().equalsIgnoreCase(cmcCoin.getId()) || iconCoin.getCoinName().equalsIgnoreCase(cmcCoin.getName()))) {
            return iconCoin.getImageUrl();
        } else if (iconCoinStar != null && (iconCoinStar.getCoinName().equalsIgnoreCase(cmcCoin.getId()) || iconCoinStar.getCoinName().equalsIgnoreCase(cmcCoin.getName()))) {
            return iconCoinStar.getImageUrl();

        } else {
            log.info("icon not found for " + cmcCoin.getId());
            return "unknown";
        }
    }

    public boolean pullFromCoinMarketCap() {
        Map<String, IconCoin> iconCoinMap = pullIcons();
        List<CMCCoin> CMCCoins;
        try {
            CMCCoins = coinMarketCapClient.getCoinList();
        } catch (IOException e) {
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "coinMarketCap api called failed " + e);
        }

        List<CoinEntity> newCoinEntities = new ArrayList<>();
        Timestamp currentTime = Timestamp.valueOf(LocalDateTime.now());
        for (CMCCoin cmcCoin : CMCCoins) {
            List<CoinEntity> coinEntities = coinRepository.findByName(cmcCoin.getId());

            if (coinEntities.size() == 1) {
                log.info("update ========== " + coinEntities.get(0).getName());
                coinRepository.updateCoin(coinEntities.get(0).getName(), cmcCoin.getMarketCap(), cmcCoin.getPrice(), cmcCoin.getVolume(), cmcCoin.getSupply(), cmcCoin.getPercentage24h(), cmcCoin.getPercentage7d(), currentTime);
            } else if (coinEntities.size() == 0) {
                log.info("cmcCoin.getSymbol()" + cmcCoin.getSymbol());

                CoinEntity newCoin = new CoinEntity();
                newCoin.setEnabled(false);
                newCoin.setSymbol(cmcCoin.getSymbol());
                newCoin.setLogo(getIconPath(iconCoinMap, cmcCoin));
                newCoin.setMarketCap(cmcCoin.getMarketCap() == null ? BigDecimal.ZERO : cmcCoin.getMarketCap());

                newCoin.setPrice(cmcCoin.getPrice() == null ? BigDecimal.ZERO : cmcCoin.getPrice());
                newCoin.setVolume24h(cmcCoin.getVolume() == null ? BigDecimal.ZERO : cmcCoin.getVolume());
                newCoin.setSupply(cmcCoin.getSupply() == null ? BigDecimal.ZERO : cmcCoin.getSupply());
                newCoin.setPercentage7d(cmcCoin.getPercentage7d() == null ? BigDecimal.ZERO : cmcCoin.getPercentage7d());
                newCoin.setPercentage24h(cmcCoin.getPercentage24h() == null ? BigDecimal.ZERO : cmcCoin.getPercentage24h());

                newCoin.setName(cmcCoin.getId());
                newCoin.setDisplayName(cmcCoin.getName());

                newCoin.setCreateTime(currentTime);
                newCoinEntities.add(newCoin);
            } else {
                throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER, "more than coin name = " + cmcCoin.getId());
            }
        }
        coinRepository.save(newCoinEntities);

        return true;
    }

    public CoinDescription getDescriptionForCoinById(long id) {
        Coin coin = Coin.buildFromEntity(coinRepository.findOne(id));
        CoinDescription coinDescription = getDescriptionForCoin(coin.getName());
        if (coinDescription.getId() == null) {
            coinDescription.setId(id);
        }
        return coinDescription;
    }

    public List<Coin> getCoinsByEnabled(boolean enabled) {
        List<Coin> coins = Coin.buildFromEntity(coinRepository.findByEnabled(enabled));
        Collections.sort(coins);
        return coins;
    }

    public List<Coin> getCoinsByEnabledAndKeyword(boolean enabled, String keyword) {
        List<Coin> coinSymbolKeyword = Coin.buildFromEntity(coinRepository.findByEnabledAndSymbolIgnoreCase(enabled, keyword));
        List<Coin> coinNameKeyword = Coin.buildFromEntity(coinRepository.findByEnabledAndNameIgnoreCase(enabled, keyword));
        List<Coin> coinSymbolKeywordCopy = new ArrayList<>(coinSymbolKeyword);
        coinSymbolKeywordCopy.removeAll(coinNameKeyword);
        coinNameKeyword.addAll(coinSymbolKeywordCopy);
        Collections.sort(coinNameKeyword);
        return coinNameKeyword;
    }
}
