package com.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.crypto.configuration.ControllerConfig;

@Import({ControllerConfig.class})
@SpringBootApplication
public class TestAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAccountApplication.class, args);
	}
}
