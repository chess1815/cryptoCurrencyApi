package com.crypto.service;

import com.crypto.TestAccountApplication;
import com.crypto.domain.Coin;
import com.crypto.domain.dto.CMCCoin;
import com.crypto.domain.dto.CoinEntity;
import com.crypto.domain.dto.IconCoin;
import com.crypto.service.client.CoinMarketCapClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAccountApplication.class)
public class CoinServiceIT {
    @Autowired
    private CoinService coinService;

    @Test
    public void pullFromCoinMarketCap() throws Exception {
        coinService.pullFromCoinMarketCap();
        List<Coin> coins = coinService.getAllCoin();
        assertThat(coins.size()).isGreaterThan(10);
    }


    @Test
    public void pullIcons() throws Exception {
        Map<String,IconCoin> map =  coinService.pullIcons();
        assertThat(map.size()).isGreaterThan(10);
    }

    @Test
    public void getIconPath(){
        Map<String,IconCoin> map =  coinService.pullIcons();
        String path = coinService.getIconPath(map, CMCCoin.builder().symbol("BTG").id("BitGem").name("BitGem").build());
        assertThat(path).isEqualTo("/media/19634/btg.png");
    }
}