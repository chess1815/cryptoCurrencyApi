package com.crypto.repository;

import com.crypto.TestAccountApplication;
import com.crypto.domain.dto.CoinEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAccountApplication.class)
@Slf4j
@Transactional
public class CoinRepositoryIT {

    @Autowired
    private CoinRepository coinRepository;

    @Test
    public void findCoinById() {
        CoinEntity result = coinRepository.findById(1);
        assertThat(result).isNotNull();
    }

    @Test
    public void findByEnabled() {
        List<CoinEntity> results = coinRepository.findByEnabled(false);
        assertThat(results).isNotEmpty();
    }

    @Test
    public void saveCoin_uniqueDate_success() {


        CoinEntity expect = new CoinEntity();
        expect.setEnabled(true);
        expect.setSymbol("BTC");
        expect.setLogo("20627/nxt");

        expect.setMarketCap(BigDecimal.ONE);
        expect.setPrice(BigDecimal.ONE);
        expect.setVolume24h(BigDecimal.ONE);
        expect.setSupply(BigDecimal.ONE);
        expect.setPercentage7d(BigDecimal.ONE);
        expect.setPercentage24h(BigDecimal.ONE);

        expect.setName("bit coin");

        expect.setEnabled(true);
        expect.setCreateTime(Timestamp.valueOf(LocalDateTime.now()));
        CoinEntity result = coinRepository.save(expect);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isGreaterThan(0);
    }

    @Test
    public void enableCoin() {
        coinRepository.updateEnabled(1,true);
    }

    @Test
    public void updateCoin() {
        coinRepository.updateCoin("fake coin", BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN,Timestamp.valueOf(LocalDateTime.now()));
    }

    @Test
    public void findByEnabledAndKeyword(){
        List<CoinEntity> results = coinRepository.findByEnabledAndSymbolIgnoreCase(false,"nontexist");
        assertThat(results).isEmpty();
    }
    @Test
    public void findByEnabledAndKeyword_exist2(){
        List<CoinEntity> results = coinRepository.findByEnabledAndSymbolIgnoreCase(false,"ETN");
        assertThat(results).isNotEmpty();
    }
}