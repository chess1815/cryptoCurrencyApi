package com.crypto.repository;

import com.crypto.TestAccountApplication;
import com.crypto.domain.dto.AccountEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAccountApplication.class)
@Slf4j
public class AccountRepositoryIT {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void getAllAccount() {
        List<AccountEntity> accountEntityList = accountRepository.getAllAccount();
        assertThat(accountEntityList.size()).isGreaterThan(0);
    }

    @Test
    public void getAccountByWechatId() {
        AccountEntity accountEntity = accountRepository.getAccountByWechatId("vdvdvdvd1");
        assertThat(accountEntity.getId()).isEqualTo(1);

    }

    @Test
    public void create() {

        AccountEntity accountEntity = new AccountEntity("adafa11", "afafafaf1111", "1333 bbd road", LocalDateTime.now(), null);
        AccountEntity result = accountRepository.create(accountEntity);

        assertThat(result.getLastActivityTime()).isNotNull();
    }

}