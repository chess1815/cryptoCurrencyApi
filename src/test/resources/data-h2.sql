INSERT INTO ACCOUNT (id, name, wechatId, address, createTime) VALUES
  (1, 'testAccount1',
   'vdvdvdvd1', '14 brovel st',
   '2017-03-03 00:00:01'),
  (2, 'testAccount2',
   'vdvdvdvd2', '232 yonge', '2017-03-03 00:00:01'),
  (3, 'testAccount3',
   'ewewewe3', '12 ottawa st', '2017-03-03 00:00:01'),
  (4, 'testAccount4',
   'vdvdvdvd4', '19 melton grove st', '2017-03-03 00:00:01');

insert into coin (id, account_enabled, logo, market_cap, name, percentage24h, percentage7d, price, supply, symbol, volume24h,display_name)
values (1, false, '20627/nxt', 1.0, 'fake coin', 1.0, 1.0, 1.0, 1.0, 'ETN', 1.0,'ETN'),
 (2, false, '20627/nxt', 1.0, 'test coin', 1.0, 1.0, 1.0, 1.0, 'ETN1', 1.0,'ETN1');


insert into coin_description (id,  name, short_description, long_description)
values (1, 'fake coin', 'ddddddddddd', 'afafafaf') ;